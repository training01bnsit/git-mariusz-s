﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CanonAppConsole
{
    // Different methods for supporting frutis and veggies processing
    class Utils
    {
        void ProcessPeach() { }
        void ProcessNuts() { }
        void ProcessGrape() { }
        void ProcessBlackberryABC() { }
        void ProcessWatermelon()
        {
            // TODO implement
        }

        void ProcessMelon() { }
        void ProcessExperiment() { }
        void ProcessAnotherExperiment() { }
        void ProcessRemote() { }
    }
}
